﻿using Csv.Serializer;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class DataLoader<T> : IDataLoader where T : class
    {
        private readonly IDataParser<List<T>> _dataParser;
        private readonly List<T> _dataList;

        public DataLoader(     
            List<T> dataList
            )
        {
            _dataList = dataList;
        }
        public void LoadData()
        {
            //???должен использовать ThreadSheduler???
        }

    }
}
