﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Csv.Serializer;
using System.Text;
using Shedulers;
using Handlers;
using System.Linq;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = @"C:\Users\xamwi\source\repos\Homework_23\Otus.Teaching.Concurrency.Import\Homework_23\bin\Debug\net6.0\customers.csv";// Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
        private static string _separator = ";";
        private static bool _skipHeaders = true;
        private static string _parsingType = "2";
        private static string _dbWritingType = "1";
        private static int _cores = Environment.ProcessorCount;

        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;

            using var dataContext = new AppDbContext();
            new DbInitializer(dataContext).Initialize();

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            Console.WriteLine($"Путь к файлу {_dataFilePath}");

            Console.WriteLine("Parsing csv data...");

            var parsingStopWatch = new Stopwatch();
            parsingStopWatch.Start();

            List<Customer> customers = new List<Customer>();

            if(_parsingType == "2")
            {
                List<List<Customer>> customersChunkList = new List<List<Customer>>();

                //var cores = 8;// Environment.ProcessorCount;

                Console.WriteLine("File reading statrted...");
                var fileReadingStWatch = new Stopwatch();
                fileReadingStWatch.Start();

                using var stream = new FileStream(_dataFilePath, FileMode.Open);
                string fileStr = ReadFile(stream);

                fileReadingStWatch.Stop();
                Console.WriteLine($"File reading stoped. Time: {fileReadingStWatch.Elapsed}");

                Console.WriteLine("String to csv array convertion statrted...");
                var toStringArrStWatch = new Stopwatch();
                toStringArrStWatch.Start();

                var strArr = fileStr.Trim().Split("\r\n");

                toStringArrStWatch.Stop();
                Console.WriteLine($"String to csv array convertion stoped. Time: {toStringArrStWatch.Elapsed}");

                var headers = new List<string>();
                headers.AddRange(strArr[0].Substring(1, strArr[0].Length - 2).Split(_separator));

                Console.WriteLine("Header deleting statrted...");
                var headerDelStWatch = new Stopwatch();
                headerDelStWatch.Start();

                strArr = strArr.Skip(1).ToArray();//удаляем заголовки

                headerDelStWatch.Stop();
                Console.WriteLine($"Header deleting stoped. Time: {headerDelStWatch.Elapsed}");

                int range = strArr.Length / _cores;

                if (strArr.Length <= 100) _cores = 1;//если строк меньше 100 исп один поток в пуле

                var dataSettingsObjects = new List<CsvParsingSettings<Customer>>();

                var startInd = 0;

                for(int i = 0; i < _cores; i++)
                {
                    List<Customer> chunkOfCustomers = new List<Customer>();
                    customersChunkList.Add(chunkOfCustomers);

                    //???проверять ли остаток и если меньше чем n то не создавать а объединять???

                    if(i == _cores - 1)
                    {
                        dataSettingsObjects.Add(new CsvParsingSettings<Customer>(chunkOfCustomers, strArr, _separator, startInd, strArr.Length - 1, true, headers));
                        break;
                    }
                    dataSettingsObjects.Add(new CsvParsingSettings<Customer> (chunkOfCustomers, strArr, _separator, startInd, startInd - 1 + range, true, headers ));

                    startInd += range;
                }

                //var schedulerForFileReading = new ThreadScheduler<CsvStringArrayHandler<Customer>, Customer>(dataSettingsObjects);
                var schedulerForFileReading = new ThreadScheduler(dataSettingsObjects, (o) =>
                {
                    var handler = HandlerFactory.GetCsvStringArrayHandler<Customer>(o);

                    handler.Handle();
                });
                schedulerForFileReading.ProcessQueue();

                Console.WriteLine("Chunk constructing statrted...");
                var chunkConstrStWatch = new Stopwatch();
                chunkConstrStWatch.Start();
                foreach(var cl in customersChunkList)
                {
                    customers.AddRange(cl);
                }
                chunkConstrStWatch.Stop();
                Console.WriteLine($"Chunk constructing stoped. Time: {chunkConstrStWatch.Elapsed}");
            }
            else
            {
                //однопоточный
                var parser = new CsvFileParser<Customer>(_dataFilePath, _separator);
                customers = parser.Parse();
            }

            parsingStopWatch.Stop();
            Console.WriteLine($"Csv data parsed. Parsing time: {parsingStopWatch.Elapsed}");

            Console.WriteLine("Loading data to Db...");

            if (_dbWritingType == "2")
            {
                int rangeForDbWriting = customers.Count / _cores;

                if (customers.Count <= 100) _cores = 1;//если строк меньше 100 исп один поток в пуле

                var dbWritingSettingsObjects = new List<DbWritingSettings<Customer>>();

                var startIndForDbWriting = 0;

                for (int i = 0; i < _cores; i++)
                {
                    if (i == _cores - 1)
                    {
                        dbWritingSettingsObjects.Add(new DbWritingSettings<Customer>(customers, startIndForDbWriting, customers.Count - 1));
                        break;
                    }
                    dbWritingSettingsObjects.Add(new DbWritingSettings<Customer>(customers, startIndForDbWriting, startIndForDbWriting - 1 + rangeForDbWriting));

                    startIndForDbWriting += rangeForDbWriting;
                }

                var schedulerForDbWriting = new ThreadScheduler(dbWritingSettingsObjects, (o) =>
                {
                    var handler = HandlerFactory.GetDbWritingHandler<Customer>(o);

                    handler.Handle();
                });
                schedulerForDbWriting.ProcessQueue();
            }
            else//однопоточная запись в БД
            {
                var repo = new Repository<Customer>(dataContext);

                Console.WriteLine("Single-thread dbWriting started...");
                var dbWriteStopWatch = new Stopwatch();
                dbWriteStopWatch.Start();

                Console.WriteLine("Adding to ef collection started...");
                var addToEFColStopWatch = new Stopwatch();
                addToEFColStopWatch.Start();

                repo.AddRange(customers);

                addToEFColStopWatch.Stop();
                Console.WriteLine($"Adding to ef collection stoped. Time: {addToEFColStopWatch.Elapsed}");

                Console.WriteLine("Adding to database started...");
                var addToDbStopWatch = new Stopwatch();
                addToDbStopWatch.Start();

                repo.Complete();

                addToDbStopWatch.Stop();
                Console.WriteLine($"Adding to database stoped. Time: {addToDbStopWatch.Elapsed}");


                dbWriteStopWatch.Stop();
                Console.WriteLine($"Single-thread dbWriting stoped. Time: {dbWriteStopWatch.Elapsed}");
            }
            //здесь исп коллекцию customers для загрузки в бд(многопоточно)

            //var loader = new DataLoader();

            //loader.LoadData();

            Console.WriteLine("Data loaded.");


        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFilePath = args[0];
                Console.WriteLine($"Путь к файлу из аргумента {_dataFilePath}");
                //Console.WriteLine(args[0]);

                //добавить валидацию сепаратора
                _separator = args[1];

                //
                if(!Boolean.TryParse(args[2], out _skipHeaders))
                {
                    Console.WriteLine("skip headers value is required");
                    return false;
                }

                //добавить валидацию типа парсинга
                _parsingType = args[3];

                //
                _dbWritingType = args[4];
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            return true;
        }

        //static void GenerateCustomersDataFile()
        //{
        //    var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);
        //    xmlGenerator.Generate();
        //}
        private static string ReadFile(FileStream stream)
        {
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);

            return Encoding.Default.GetString(buffer);

        }

    }
}