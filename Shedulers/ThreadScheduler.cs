﻿using Handlers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shedulers
{
    //public class ThreadScheduler<THanlder, T> : IScheduler
    //    where THanlder : IHandler
    //    where T : class
    public class ThreadScheduler : IScheduler
    {
        private readonly IEnumerable<object> _objects;
        private readonly Action<object> _action;

        public ThreadScheduler(IEnumerable<object> objects, Action<object> action)
        {
            _objects = objects;
            _action = action;
        }

        public void ProcessQueue()
        {
            int count = _objects.Count();

            WaitHandle[] waitHandles = new WaitHandle[count];

            int i = 0;
            foreach(var obj in _objects)
            {

                var stateObj = new ThreadPoolItem(i, obj);

                ThreadPool.QueueUserWorkItem(WaitCallback, stateObj);

                waitHandles[i] = stateObj.WaitHandle;

                i++;
            }
            WaitHandle.WaitAll(waitHandles);
        }

        private void WaitCallback(object? state)
        {
            Console.WriteLine("WaitCallback executing started...");
            var stW = new Stopwatch();
            stW.Start();

            var item = (ThreadPoolItem)state;

            //var handler = HandlerFactory.GetHandlerWithActivator<THanlder>(item.Data);
            //var handler = HandlerFactory.GetHandler<T>(item.Data);

            //handler.Handle();
            _action.Invoke(item.Data);

            var autoResetEvent = (AutoResetEvent)item.WaitHandle;

            autoResetEvent.Set();

            stW.Stop();
            Console.WriteLine($"WaitCallback executing stoped. Time: {stW.Elapsed}");
        }
    }
}
