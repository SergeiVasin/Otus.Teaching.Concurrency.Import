﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shedulers
{
    public interface IScheduler
    {
        void ProcessQueue();
    }
}
