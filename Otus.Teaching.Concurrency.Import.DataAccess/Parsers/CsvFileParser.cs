﻿using Csv.Serializer;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvFileParser<T> : IDataParser<List<T>> where T : class
    {
        private readonly string _filePath;
        private readonly string _separator;
        private readonly bool _skipHeaders;
        private readonly IEnumerable<string> _headers;

        public CsvFileParser(
            string filePath,
            string separator, 
            bool skipHeaders = false, 
            IEnumerable<string> headers = null
            )
        {
            _filePath = filePath;
            _separator = separator;
            _skipHeaders = skipHeaders;
            _headers = headers;
        }
        public List<T> Parse()
        {
            var serializer = new CsvSerializer();

            using var stream = new FileStream(_filePath, FileMode.Open);

            return serializer.Deserialize<T>(stream, _separator, _skipHeaders, _headers);
        }

    }
}
