﻿using Csv.Serializer;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvStringArrayParser<T> : IDataParser<List<T>> where T : class
    {
        private readonly CsvParsingSettings<T> _parsingSettings;

        public CsvStringArrayParser(
            CsvParsingSettings<T> parsingSettings
            )
        {
            _parsingSettings = parsingSettings;
        }

        public List<T> Parse()
        {
            //Console.WriteLine("Parsing started...");

            var serializer = new CsvSerializer();

            var collection = new List<T>();

            for (int i = _parsingSettings.StartIndex; i <= _parsingSettings.FinishIndex; i++)
            {
                collection.Add( 
                    serializer.Deserialize<T>(
                        _parsingSettings.Strings[i], 
                        _parsingSettings.Separator,
                        _parsingSettings.Headers
                        ) 
                    );
            }

            //Console.WriteLine("Parsing finished...");
            return collection;
        }

    }
}
