﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class AppDbContext : DbContext
    {
        //public AppDbContext()
        //{
        //    Database.EnsureDeleted();
        //    Database.EnsureCreated();
        //}
        public DbSet<Customer> Customers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseNpgsql("Host=localhost;Port=5433;Username=postgres;Password=123;Database=customers_lesson23");
            optionsBuilder.UseSqlite("Data source=customers.db");
        }
    }
}
