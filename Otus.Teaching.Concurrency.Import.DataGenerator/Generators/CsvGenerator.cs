﻿using Csv.Serializer;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _filePath;
        private readonly int _dataCount;
        private readonly string _separator;

        public CsvGenerator(string filePath, string separator, int dataCount)
        {
            _filePath = filePath;
            _dataCount = dataCount;
            _separator = separator;
        }
        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.Create(_filePath);
            new CsvSerializer().Serialize(_separator, stream, customers);
        }
    }
}
