﻿

using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Diagnostics;

namespace Homework_23 
{
    class Program
    {
        private static string _separator = ";";
        private static int _dataAmount = 1000000;
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
        private static string _dataFilePathProc = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers");
        private const string HandlerGeneratingProcessFileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        private const string HandlerGeneratingProcessDirectory = @"C:\Users\xamwi\source\repos\HomeWork_23\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1";
        private const string HandlerLoadingProcessFileName = "Otus.Teaching.Concurrency.Import.Loader.exe";
        private const string HandlerLoadingProcessDirectory = @"C:\Users\xamwi\source\repos\HomeWork_23\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.Loader\bin\Debug\netcoreapp3.1";
               
        
        
        
        public static void Main(string[] args)
        {
            //generate, serialize and write data to file
            var inputRes = ChooseCommandForGenerateCustomers();
            if (inputRes == "1")
            {
                GenerateCustomersDataFile();
            }
            if (inputRes == "2")
            {
                var proc = StartGeneratingProcess(_dataFilePathProc);
                proc.WaitForExit();
            }

            //parse data from file and write to db
            string parsingType = ChooseCommandForParsing();
            string dbWritingType = ChooseCommandForDbWriting();
            StartLoadingProcess(_dataFilePath, parsingType, dbWritingType).WaitForExit();




            ///////////////////////////////////////////////////////////
            //Все что ниже перенести в Loader

            ////parse data from file without threads
            //var parser = new CsvParser<Customer>(_dataFilePath, ";");//решить проблему пустого файла

            //var parsStopWatch = new Stopwatch();

            //Console.WriteLine();
            //Console.WriteLine("Parser starts...");
            //parsStopWatch.Start();
            //var customers = parser.Parse();

            //parsStopWatch.Stop();
            //Console.WriteLine($"Parser stop. Parsing time: {parsStopWatch.Elapsed}");

            ////parse data from file with threads
            //var fileReadSheduler = new FileReadThreadSheduler();


            //load data to db
            //исп Loader процесс
            //в loader сделать парсирование данных из файла(по выбору многопоточно или нет)
            //и многопоточную загрузку данных в БД
            //получается надо вынести Shedulers, Handlers в Loader проект

        }
        static string ChooseCommandForGenerateCustomers()
        {
            while (true)
            {

                Console.WriteLine("Генерация файла csv.");
                Console.WriteLine("Выберите команду.");
                Console.WriteLine("----------------");
                Console.WriteLine("1. С помощью метода.");
                Console.WriteLine("2. С помощью процесса.");
                Console.WriteLine("3. Не выполнять генерацию данных.");

                string choiceResult = "";
                try
                {
                    choiceResult = Console.ReadLine();
                    if (choiceResult != "1" && choiceResult != "2" && choiceResult != "3") throw new Exception();
                }
                catch
                {
                    Console.WriteLine("Введите номер команды!");
                    continue;
                }
                return choiceResult;
            }
        }
        static void GenerateCustomersDataFile()
        {
            var csvGenerator = new CsvGenerator(_dataFilePath, _separator, _dataAmount);
            csvGenerator.Generate();
        }

        private static Process StartGeneratingProcess(string path)
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { path, _separator, _dataAmount.ToString() },
                FileName = GetPathToHandlerProcess(HandlerGeneratingProcessDirectory, HandlerGeneratingProcessFileName),
            };

            var process = Process.Start(startInfo);
            Console.WriteLine($"Generating process Id: {process.Id}");

            return process;
        }
        private static string GetPathToHandlerProcess(string directory, string fileName)
        {
            return Path.Combine(directory, fileName);
        }

        static string ChooseCommandForParsing()
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Парсинг файла csv.");
                Console.WriteLine("Выберите команду.");
                Console.WriteLine("----------------");
                Console.WriteLine("1. Однопоточный парсинг.");
                Console.WriteLine("2. Многопоточный парсинг.");

                string choiceResult = "";
                try
                {
                    choiceResult = Console.ReadLine();
                    if (choiceResult != "1" && choiceResult != "2") throw new Exception();
                }
                catch
                {
                    Console.WriteLine("Введите номер команды!");
                    continue;
                }
                return choiceResult;
            }
        }

        static string ChooseCommandForDbWriting()
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Запсиь данных в базу данных.");
                Console.WriteLine("Выберите команду.");
                Console.WriteLine("----------------");
                Console.WriteLine("1. Однопоточная запись.");
                Console.WriteLine("2. Многопоточная запись.");

                string choiceResult = "";
                try
                {
                    choiceResult = Console.ReadLine();
                    if (choiceResult != "1" && choiceResult != "2") throw new Exception();
                }
                catch
                {
                    Console.WriteLine("Введите номер команды!");
                    continue;
                }
                return choiceResult;
            }
        }

        private static Process StartLoadingProcess(string path, string parsingType, string dbWritingType)
        {

            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { path, _separator, "true", parsingType, dbWritingType },
                FileName = GetPathToHandlerProcess(HandlerLoadingProcessDirectory, HandlerLoadingProcessFileName),
            };

            var process = Process.Start(startInfo);
            Console.WriteLine($"Loading process Id: {process.Id}");

            return process;
        }
    }
}