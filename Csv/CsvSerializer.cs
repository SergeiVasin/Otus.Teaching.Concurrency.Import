﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Csv.Serializer
{
    public class CsvSerializer
    {
        public CsvSerializer()
        {

        }
        public void Serialize<T>( string separator, FileStream stream, IEnumerable<T> objList)
        {
            string str = Serialize(separator, objList);
            WriteToCsvFile(stream, str);
        }

        public List<T> Deserialize<T>(FileStream stream, string separator, bool skipHeaders = false, IEnumerable<string> headers = null)
        {
            //вместо этого блока можно пройтись построчно в stream вызывая соотв метод и на каждой строке(в цикле) выполнять действия
            //в json видимо есть стеки скобок, куда при обнаружении они складываются и коллекции содержимого. После чего идет преобразование в дерево объектов
            string str = ReadFile(stream);
            var strObjArr = str.Trim().Split("\r\n");
            var objList = new List<T>();

            bool skipHeaderStr = skipHeaders;
            List<string> iHeaders = new List<string>();
            if (skipHeaderStr)
            {
                iHeaders.AddRange(headers);
                skipHeaderStr = false;
            }

            Type type = typeof(T);

            foreach(var item in strObjArr)//строка неразобранная
            {
                var sArr = item.Substring(1, item.Length - 2).Split(separator);//разобранная строка

                if (!skipHeaderStr)
                {
                    iHeaders.AddRange(sArr);
                    skipHeaderStr = true;
                    continue;
                }

                var newObj = (T)Activator.CreateInstance(type);

                for(int i = 0; i < iHeaders.Count; i++)
                {
                    //newObj.GetType().GetProperty(iHeaders[i]).SetValue(newObj, sArr[i]);

                    var pInfo = newObj.GetType().GetProperty(iHeaders[i]);
                    Type t = pInfo.PropertyType;
                    TypeConverter conv = TypeDescriptor.GetConverter(t);

                    var convertResult = conv.ConvertFrom(sArr[i]);

                    pInfo.SetValue(newObj, convertResult);
                }

                objList.Add(newObj);
            }

            return objList;
        }

        public List<T> Deserialize<T>(string[] stringArray, string separator, bool skipHeaders = false, IEnumerable<string> headers = null)
        {
            //вместо этого блока можно пройтись построчно в stream вызывая соотв метод и на каждой строке(в цикле) выполнять действия
            //в json видимо есть стеки скобок, куда при обнаружении они складываются и коллекции содержимого. После чего идет преобразование в дерево объектов
            
            var strObjArr = stringArray;

            var objList = new List<T>();

            bool skipHeaderStr = skipHeaders;
            List<string> iHeaders = new List<string>();
            if (skipHeaderStr)
            {
                iHeaders.AddRange(headers);
                skipHeaderStr = false;
            }

            Type type = typeof(T);

            foreach (var item in strObjArr)//строка неразобранная
            {
                var sArr = item.Substring(1, item.Length - 2).Split(separator);//разобранная строка

                if (!skipHeaderStr)
                {
                    iHeaders.AddRange(sArr);
                    skipHeaderStr = true;
                    continue;
                }

                var newObj = (T)Activator.CreateInstance(type);

                for (int i = 0; i < iHeaders.Count; i++)
                {
                    //newObj.GetType().GetProperty(iHeaders[i]).SetValue(newObj, sArr[i]);

                    var pInfo = newObj.GetType().GetProperty(iHeaders[i]);
                    Type t = pInfo.PropertyType;
                    TypeConverter conv = TypeDescriptor.GetConverter(t);

                    var convertResult = conv.ConvertFrom(sArr[i]);

                    pInfo.SetValue(newObj, convertResult);
                }

                objList.Add(newObj);
            }

            return objList;
        }

        public T Deserialize<T>(string str, string separator, IEnumerable<string> headers)
        {
            var newObj = (T)Activator.CreateInstance(typeof(T));
            var sArr = str.Substring(1, str.Length - 2).Split(separator);//разобранная строка

            int i = 0;
            foreach (var header in headers)
            {
                var pInfo = newObj.GetType().GetProperty(header);
                Type t = pInfo.PropertyType;

                TypeConverter conv = TypeDescriptor.GetConverter(t);
                var convertResult = conv.ConvertFrom(sArr[i]);

                pInfo.SetValue(newObj, convertResult);

                i++;
            }
            return newObj;
        }

        public string Serialize<T>(string separator, IEnumerable<T> objList )
        {
            if (objList == null) return null;
            bool isFirstObj = true;
            var stringBuilder = new StringBuilder();
            List<string> csvHeadersStr = new List<string>();

            Console.WriteLine("Serialize stopwatch start...");
            var mainStopWatch = new Stopwatch();
            mainStopWatch.Start();

            foreach (var obj in objList)
            {
                stringBuilder.Append("\"");

                var propArr = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);//рефлексия
                List<string> strArr = new List<string>();


                foreach (var pi in propArr)
                {
                    if (pi.PropertyType.BaseType == typeof(object) && pi.PropertyType != typeof(string)) continue;

                    if (isFirstObj)
                        csvHeadersStr.Add(pi.Name);

                    strArr.Add(pi.GetValue(obj)?.ToString());
                }

                if (isFirstObj)
                {
                    stringBuilder.Append(string.Join(separator, csvHeadersStr));
                    stringBuilder.Append("\"\r\n");
                    stringBuilder.Append("\"");
                }

                isFirstObj = false;

                stringBuilder.Append(string.Join(separator, strArr));

                stringBuilder.Append("\"\r\n");

            }

            mainStopWatch.Stop();
            Console.WriteLine($"Serialize stopwatch period {mainStopWatch.Elapsed}...");

            return stringBuilder.ToString();
        }

        private string ReadFile(FileStream stream)
        {
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);

            return Encoding.Default.GetString(buffer);

        }
        private void WriteToCsvFile(FileStream stream, string str)
        {
            byte[] buffer = Encoding.Default.GetBytes(str);

            stream.Write(buffer, 0, buffer.Length);
        }

        public async Task<List<string[]>> ReadFileToStringArrayListAsync(string separator, string filePath)
        {
            List<string[]> strArrList = new List<string[]>();

            using (StreamReader reader = new StreamReader(filePath))
            {
                string? line;

                while ((line = await reader.ReadLineAsync()) != null)
                {
                    strArrList.Add(line.Substring(1, line.Length - 2).Split(separator));
                }
            }

            return strArrList;
        }

        private List<Dictionary<string, string>> CreateKeyValueObjects(List<string[]> csvRows, bool skipHeaders)
        {
            List<Dictionary<string, string>> csvObjs = new List<Dictionary<string, string>>();
            List<string> keys = new List<string>();
            bool firstStr = true;

            foreach (var strArr in csvRows)//строки - объекты
            {
                var csvObj = new Dictionary<string, string>();

                if (!skipHeaders && firstStr)
                {
                    keys.AddRange(strArr);
                    firstStr = false;
                    continue;
                }
                for (int i = 0; i < keys.Count; i++)
                {
                    csvObj.Add(keys[i], strArr[i]);
                }
                csvObjs.Add(csvObj);
            }
            return csvObjs;
        }

        private List<T> MapKeyValueObjectsToObjects<T>(List<Dictionary<string, string>> keyValueObj)
        {
            List<T> listToReturn = new List<T>();

            //СОПОСТАВЛЯЮ Person с csvObjProps и создаю инстансы объектов
            foreach (var csvObj in keyValueObj)
            {
                Type type = typeof(T);
                var tInstance = (T)Activator.CreateInstance(type);

                foreach (PropertyInfo pi in tInstance.GetType().GetProperties())
                {
                    Type t = tInstance.GetType().GetProperty(pi.Name).PropertyType;
                    TypeConverter conv = TypeDescriptor.GetConverter(t);

                    var findResValue = csvObj.FirstOrDefault(x => x.Key == pi.Name).Value;

                    if (findResValue == null) continue;

                    var convertResult = conv.ConvertFrom(findResValue);

                    tInstance.GetType().GetProperty(pi.Name).SetValue(tInstance,
                        convertResult);
                }
                listToReturn.Add(tInstance);
            }
            return listToReturn;
        }

        public async Task<List<T>> DeserializefromFileAsync<T>(string filePath, string separator, bool skipHeaders)
        {
            var strList = await ReadFileToStringArrayListAsync(separator, filePath);// попробовать прочитать из файла не строками а целиком
                                                                                    //также сделать парсер. ???Куда его воткнуть?
            var keyValueObjs = CreateKeyValueObjects(strList, skipHeaders);
            return MapKeyValueObjectsToObjects<T>(keyValueObjs);
        }

    }
}