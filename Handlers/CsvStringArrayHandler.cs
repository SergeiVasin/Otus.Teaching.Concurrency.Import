﻿
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Handlers
{
    public class CsvStringArrayHandler<T> : IHandler where T : class
    {
        //private readonly IDataParser<List<T>> _parser;
        private readonly object _data;

        public CsvStringArrayHandler(object data)//IDataParser<List<T>> parser, 
        {
            //_parser = parser;
            _data = data;
        }
        public void Handle()
        {
            //Console.WriteLine("handling started...");

            var data = (CsvParsingSettings<T>)_data;

            var parser = new CsvStringArrayParser<T>(data);
            data.DataList.AddRange(parser.Parse());

            //Console.WriteLine("handling finished...");

        }
    }
}
