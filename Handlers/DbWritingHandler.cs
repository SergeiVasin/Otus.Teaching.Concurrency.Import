﻿using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Handlers
{
    public class DbWritingHandler<T> : IHandler where T : class
    {
        private readonly DbWritingSettings<T> _data;

        public DbWritingHandler(object data) 
        {
            _data = (DbWritingSettings<T>)data;
        }
        public void Handle()
        {

            using AppDbContext context = new AppDbContext();
            var repo = new Repository<T>(context);


            for (int i = _data.StartIndex; i <= _data.FinishIndex; i++)
            {
                repo.Add(_data.DataList[i]);

            }
            var saveRes = false;
            while(!saveRes)
            {
                try
                {
                    repo.Complete();
                    saveRes = true;
                }
                catch
                {
                    continue;
                }
            }

        }
    }
}
