﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Handlers
{
    public class HandlerFactory
    {
        public static IHandler GetHandlerWithActivator<THandler>(object data)
            where THandler : IHandler
        {

            var handler = (THandler)Activator.CreateInstance(typeof(THandler), data);

            return handler;
        }

        public static IHandler GetCsvStringArrayHandler<T>(object data) where T : class
        {
            var handler = new CsvStringArrayHandler<T>(data);

            return handler;
        }
        //public static CsvStringArrayHandler<T> GetHandler<T>(object data) where T : class
        //{
        //    var handler = new CsvStringArrayHandler<T>(data);

        //    return handler;
        //}
        public static IHandler GetDbWritingHandler<T>(object data) where T : class
        {
            var handler = new DbWritingHandler<T>(data);

            return handler;
        }

    }
}
